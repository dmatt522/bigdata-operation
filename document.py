from __future__ import division, unicode_literals

from datetime import datetime
from dateutil.parser import parse


def format_date(obj):
    mili = datetime.strftime(obj, "%f")[:3]
    return datetime.strftime(obj, "%Y-%m-%dT%H:%M:%S.{}%z".format(mili))


def status_picklist_mapper(obj):
    list_map = {
        'Legacy Value': 'Mapped Value',
        'Martketing Nurture': 'Marketing Nurture',
        'Marketing Nurturing': 'Marketing Nurture',
        'MQL': 'Untouched',
        'agreeable to receive only emails': 'Marketing Nurture',
        'No Interest Today': 'Marketing Nurture',
        'Working': 'Pursuing',
        'Do Not Call': 'Marketing Nurture',
        'Unable To Reach': 'Marketing Nurture',
        'Unqualified': 'Closed',
        'Duplicate Contact': 'Closed',
        'Competitor': 'Closed',
        'Additional Contact': 'Marketing Nurture',
        'Contacted': 'Untouched',
        'Released to Reseller': 'Marketing Nurture',
        'Rejected By Partner': 'Marketing Nurture',
        'Request for Conversion': 'Pursuing',
        'Attended': 'Untouched',
        'Left Company': 'Closed',
        'Customer': 'Untouched',
        'Partner/Consultant': 'Marketing Nurture',
        'Re-Direct Inquiry': 'Marketing Nurture',
        'Bad Data': 'Closed',
        'Redirect Inquiry': 'Marketing Nurture',
        'Existing Opportunity': 'Marketing Nurture',
        'si asiste': 'Marketing Nurture',
        'Customer Upgrade': 'Marketing Nurture',
        'Not A Good Fit': 'Marketing Nurture'
    }
    return list_map.get(obj, obj)

mappers = {
    'String': (str, str),
    'DateTime': (parse, format_date),
    'Boolean': (bool, bool),
    'StatusPicklist': (status_picklist_mapper, str),
    'Integer': (lambda x: int(x) if x else 0, float)
}

fields = {
    'email': ('Email', 'String', None),
    'last_modified_date': ('LastModifiedDate', 'DateTime', None),
    'created_date': ('CreatedDate', 'DateTime', None),
    'sales_ready_lead': ('Sales_Ready_Lead__c', 'Boolean', None),
    'status': ('Status', 'StatusPicklist', None),
    'is_converted': ('IsConverted', 'Boolean', False),
    'lead_score': ('mkto2__Lead_Score__c', 'Integer', 0),
    'lead_source': ('LeadSource', 'String', None),
    'lead_owner': ('OwnerId', 'String', None),
    'record_type_id': ('RecordTypeId', 'String', None),
    'last_interesting_type': ('mkto_si__last_interesting_moment_type__c',
                              'String', None)
}

ignored_fields = [
    'Age__c',
    'LastModifiedDate',
    'Name',
    'CreatedById',
    'BU__c',
    'mkto_si__Urgency__c',
    'mkto_si__Relative_Score__c',
    'Days_Since_Last_Activity__c',
    'Valid_Reseller__c',
    'CAM_PMFKey__c',
    'CreatedDate',
    'Opp_Source1__c',
    'mkto_si__Add_to_Marketo_Campaign__c',
    'X18_digit_ID__c',
    'BANT_counter__c',
    'mkto_si__View_in_Marketo__c',
    'Activity_Monitor__c',
    'SystemModstamp',
    'LastActivityDate',
    'Technology_Partner_formula__c',
    'LastTransferDate',
    'LastModifiedById',
    'mkto_si__Last_Interesting_Moment__c',
    'Segment1__c',
    'Commercial_Name_for_Email__c',
    'Reseller_Name__c',
    'Reseller_CPMS_ID__c',
    'CPMS_Comm_Account_ID__c',
    'Reseller_Qualified__c',
    'IsConverted',
    'Reseller_Contact_CPMS_ID__c',
    'Reseller_Contact_Email__c',
    'ConvertedDate',
    'Reseller_Contact_Last_Name__c',
    'Reseller_Contact_First_Name__c',
    'LastViewedDate',
    'LastReferencedDate',
    'PartnerAccountId',
    'Reseller_Contact_Phone__c',
    'Address',
    'PhotoUrl',
    'JigsawContactId'
]


class Document(object):
    """Base Document instance."""
    __raw_data = None

    def keys(self):
        return self.__raw_data.keys()

    def has_raw(self, key):
        return key in self.__raw_data

    def has(self, key):
        return fields[key][0] in self.__raw_data

    def has_raw_not_empty(self, key):
        return bool(self.__raw_data.get(key))

    def get_raw(self, key):
        return self.__raw_data[key]

    def __init__(self, raw_data):
        self.__raw_data = raw_data

    def __getattr__(self, key):
        field = fields.get(key)
        if not field:
            raise Exception('No info about field - {}'.format(key))
        field_raw_name, field_type, default_value = field
        mapper = mappers[field_type][0]
        if field_raw_name in self.__raw_data:
            value = mapper(self.__raw_data[field_raw_name])
        else:
            value = default_value
        return value
