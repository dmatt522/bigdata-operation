from __future__ import division, unicode_literals

import os
import datetime

import click
import pytz


USE_PROXY = False

SALESFORCE_USERNAME = 'ringlead@ca.com.fsb1'
SALESFORCE_PASSWORD = 'ca_Ring1%'
SALESFORCE_SECURITY_TOKEN = 'yGiD8Nmq5ahvjLMTSLiCkqgG'
SALESFORCE_MONGO_DB = 'sf_dedup'
SALESFORCE_REDIS_DB = 10
SALESFORCE_SANDBOX = True
SALESFORCE_ORG_ID = '00D29000000CvwC'
SALESFORCE_PROXIES = {
    'http': 'http://hellysmile.info:3128',
    'https': 'http://hellysmile.info:3128'
}

HUGE_DUPS_AMOUNT = 100

SPECIAL_RECORD_TYPES = [
    '01230000000Dhu2AAC', '01230000000yspfAAA'
]

LIMIT_DATE = datetime.datetime(2013, 8, 1).replace(tzinfo=pytz.UTC)

CSV_DATA_FILE = 'data/500k.csv'
CSV_EXCEPTIONS_FILE = 'data/exceptions.csv'
CSV_POST_UPDATE_FILE = 'data/post_update.csv'
CSV_TEST_INPUT_FILE = 'data/input_test_data.csv'
CSV_TEST_OUTPUT_FILE = 'data/output_test_data.csv'

SLEEP_TIMEOUT = 5

if os.environ.get('PROD'):
    from config_prod import *
    click.echo('Using PRODUCTION mode')
else:
    click.echo('Using SANDBOX mode')

click.echo('SALESFORCE_USERNAME is {}'.format(SALESFORCE_USERNAME))
