from __future__ import division, unicode_literals

from simple_salesforce import Salesforce


def get_prod_auth(username, password, organizationId, sandbox=False):
    """
    Establish connection with production instance of Salesforce.
    """

    auth = Salesforce(
        username=username,  # ex. 'https://na16.salesforce.com'
        password=password,
        organizationId=organizationId,
        sandbox=sandbox,    # Boolean value
        )
    return auth


auth = get_prod_auth(username='ringlead@ca.com.fsb1',
                     password='ca_Ring1%', organizationId='00D29000000CvwC',
                     sandbox=True)

lead_id = '00Qa0000013YMtKEAW'
new_owner_id = '00530000000whOEAAY'

auth.Lead.update(lead_id, {'OwnerId': new_owner_id})
lead_from_db = auth.Lead.get(lead_id)
print('New lead owner from DB: {}'.format(lead_from_db['OwnerId']))
