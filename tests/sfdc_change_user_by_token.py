from __future__ import division, unicode_literals

from simple_salesforce import Salesforce


def get_prod_auth(instance_url, access_token, sandbox=False):
    """
    Establish connection with production instance of Salesforce.
    """

    auth = Salesforce(
        instance_url=instance_url,  # ex. 'https://na16.salesforce.com'
        session_id=access_token,
        sandbox=sandbox,    # Boolean value
        )
    return auth


auth = get_prod_auth(instance_url='https://test.salesforce.com',
                     access_token='yGiD8Nmq5ahvjLMTSLiCkqgG',
                     sandbox=True)

lead_id = '00Qa0000013YMtKEAW'
new_owner_id = '00530000000whOEAAY'

auth.Lead.update(lead_id, {'OwnerId': new_owner_id})
lead_from_db = auth.Lead.get(lead_id)
print('New lead owner from DB: {}'.format(lead_from_db['OwnerId']))
