#!/usr/bin/env python
from __future__ import division, unicode_literals

import click

import commands


def monkey_exit():
    raise SystemExit()


def monkey_fail(msg):
    click.echo(msg)
    click.exit()


click.exit = monkey_exit
click.fail = monkey_fail


@click.group()
def cli():
    pass


@cli.command('dump')
def dump():
    commands.dump()


@cli.command('mergeone')
@click.option('--email', help='Lead to merge by Email', required=True)
def mergeone(email):
    commands.mergeone(email)


@cli.command('mergeall')
def mergeall():
    commands.mergeall()


@cli.command('wipe')
def wipe():
    commands.wipe()


@cli.command('test_records')
def test_records():
    commands.test_records()


@cli.command('get_field_value')
@click.option('--leadid', help='Lead ID', required=True)
@click.option('--fields', help='Lead fields name', required=True)
def get_field_value(leadid, fields):
    commands.get_field_value(leadid, fields)


@cli.command('get_field_value_by_email')
@click.option('--email', help='Lead Email', required=True)
@click.option('--field', help='Lead field name', required=True)
def get_field_value_by_email(email, field):
    commands.get_field_value_by_email(email, field)


@cli.command('dump_from_db')
@click.option('--email', help='Lead Email', required=True)
@click.option('--fields', help='Lead fields name', required=True)
def dump_from_db(email, fields):
    commands.dump_from_db(email, fields)


@cli.command('dump_all_from_db')
@click.option('--fields', help='Lead fields name', required=True)
def dump_all_from_db(fields):
    commands.dump_all_from_db(fields)


@cli.command('delete')
@click.option('--email', help='Lead Email', required=True)
def delete(email):
    commands.delete(email)


@cli.command('count_records')
def count_records():
    commands.count_records()


@cli.command('get_dups_count')
@click.option('--email', help='Lead Email', required=True)
def get_dups_count(email):
    commands.get_dups_count(email)


@cli.command('load')
@click.option('--email', help='Lead Email', required=True)
def load(email):
    commands.load(email)


if __name__ == '__main__':
    try:
        cli()
    finally:
        from app import report_file

        report_file.close()
