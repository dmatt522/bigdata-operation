from __future__ import division, unicode_literals

import click

from app import report
from document import Document, fields
from special_rules import apply_rule


def select_master_and_slaves(doc_dups):
    """Select master and slaves IDS."""
    master_id = None
    if not doc_dups:
        return None, []

    sales_ready_dups = [dup[0] for dup in doc_dups.iteritems()
                        if dup[1].sales_ready_lead]

    if len(sales_ready_dups) == 1:
        master_id = sales_ready_dups[0]
    elif len(sales_ready_dups) > 1:
        report.writerow(['many_sales_ready', sales_ready_dups])

        click.echo(
            'Exception: found > 1 "Sales Ready": {}'.format(sales_ready_dups)
        )
        return None, []
    else:
        master_id = sorted(
            doc_dups.iteritems(),
            key=lambda x: x[1].last_modified_date,
            reverse=True
        )[0][0]

    dup_sorted = sorted(
        doc_dups.iteritems(),
        key=lambda x: x[1].last_modified_date,
        reverse=True
    )
    slave_ids = [dup[0] for dup in dup_sorted if dup[0] != master_id]

    return master_id, slave_ids


def merge_dups(doc_dups, master_id, slave_ids):
    """Merge dups body using general rules."""
    if not doc_dups or master_id is None:
        return {}

    master_doc = doc_dups[master_id]
    body = {}

    # Get all raw fields.
    raw_field_names = set()
    for dup in doc_dups.itervalues():
        raw_field_names.update(set(dup.keys()))

    # Special rules.
    special_fields = {
        'status', 'lead_score', 'lead_source', 'lead_owner',
        'last_interesting_type'
    }
    for special_field in special_fields:
        data, status = apply_rule(special_field, doc_dups,
                                  master_id, slave_ids)

        if not status:
            click.fail('Abort on {} field for {}'.format(
                special_field, master_doc.email
            ))
            return None

        click.echo(
            'Master ID {}. Email {}. Special rule data: {}'.format(
                master_id, master_doc.email, data
            )
        )
        body.update(data)

    # Base (default) rule.
    ignore_fields = {fields[i][0] for i in special_fields}
    for raw_field_name in raw_field_names - ignore_fields:
        if master_doc.has_raw_not_empty(raw_field_name):
            body[raw_field_name] = master_doc.get_raw(raw_field_name)
        else:
            for slave_id in slave_ids:
                slave_doc = doc_dups[slave_id]
                if slave_doc.has_raw_not_empty(raw_field_name):
                    body[raw_field_name] = slave_doc.get_raw(raw_field_name)
                    break

    return body


def use_rules(dups):
    """Merge dupicates documents to on `body` document."""
    # Make `Document` instance from dict.
    doc_dups = {key: Document(value) for key, value in dups.iteritems()}

    # Master ID, list of slaves IDs.
    master_id, slave_ids = select_master_and_slaves(doc_dups)

    # Result body values.
    body = merge_dups(doc_dups, master_id, slave_ids)

    return master_id, slave_ids, body, dups.get(master_id)
