from __future__ import division, unicode_literals

import csv
from itertools import groupby
from copy import deepcopy

import click

import config
import queries

from utils import escape, LeadState
from app import sf, db, report, r
from rules import use_rules
from document import Document, ignored_fields


def dump():
    total_rows = 0
    emails = set()
    new = 0

    csv_file = open(config.CSV_DATA_FILE, 'rb')

    csv_reader = csv.reader(csv_file, delimiter=b',')
    next(csv_reader, None)  # skip the headers

    for row in csv_reader:
        total_rows += 1
        emails.add(row[16])

    click.echo('Processed {} rows from csv'.format(total_rows))
    click.echo('Found {} unique emails'.format(len(emails)))

    with click.progressbar(emails, label='Storing to db') as bar:
        for email in bar:
            exist = db.leads.find_one({
                'email': email
            })

            if exist is None:
                lead = {
                    'master': None,
                    'email': email,
                    'dups': {},
                    'body': {},
                    'initial_body': {},
                    'state': LeadState.START
                }

                new += 1

                db.leads.insert(lead)

    click.echo('{} emails existed at DB'.format(len(emails) - new))

    click.echo('{} new emails stored to DB'.format(new))


def mergeone(lead):
    if not isinstance(lead, dict):
        db_lead = db.leads.find_one({
            'email': lead
        })

        if db_lead is None:
            click.fail('Lead is not found {}'.format(lead))
        else:
            lead = db_lead

    query = {
        'email': lead['email']
    }

    try:
        lock_key = 'sf_lock:{}'.format(query['email'])
        lock = r.setnx(lock_key, 0)
        if lock != 1:
            raise StopIteration
    except StopIteration:
        pass
    else:
        if not lead['dups']:
            records = sf.query_all(queries.GET_BY_EMAIL.format(
                email=escape(lead['email'])
            ))['records']

            amount = len(records)

            if not amount:
                db.leads.update(
                    query,
                    {
                        '$set': {
                            'state': LeadState.EMPTY
                        }
                    }
                )

                click.fail('Can not find Lead by email')
            elif amount > config.HUGE_DUPS_AMOUNT:

                report.writerow(['too_many_dups', lead['email']])

                db.leads.update(
                    query,
                    {
                        '$set': {
                            'state': LeadState.CAPACITY
                        }
                    }
                )

                click.echo('Found {} dups for {}'.format(amount, lead['email']))
            elif amount == 1:
                db.leads.update(
                    query,
                    {
                        '$set': {
                            'state': LeadState.SINGLE
                        }
                    }
                )

                click.fail('No dups found')
            else:
                click.echo('Found {} dups for {}'.format(amount, lead['email']))

                for record in records:
                    body = sf.Lead.get(record['Id'])

                    doc = Document(body)

                    if doc.created_date >= config.LIMIT_DATE:
                        click.echo('Limit date. ID {}'.format(record['Id']))
                        continue

                    if doc.record_type_id in config.SPECIAL_RECORD_TYPES:
                        report.writerow(['record_type_id_skip', record['Id']])
                        click.echo('Skip special RecordTypeId.')
                        continue

                    if doc.is_converted:
                        click.echo('IsConverted True. ID {}'.format(record['Id']))
                        continue

                    # TODO: double check MasterRecordId ???
                    for skip in ['Id', 'IsDeleted']:
                        del body[skip]

                    lead['dups'][record['Id']] = body

                    db.leads.update(
                        query,
                        {
                            '$set': {
                                'dups': lead['dups']
                            }
                        }
                    )

        if (
            lead['master'] is None
            or
            lead['body'] == {}
            or
            lead['initial_body'] == {}
        ):
            master_id, slaves_ids, body, initial_body = use_rules(lead['dups'])

            if body is None:
                return

            lead['master'] = master_id
            lead['body'] = body
            lead['initial_body'] = initial_body

            db.leads.update(
                query,
                {
                    '$set': {
                        'master': lead['master'],
                        'body': lead['body'],
                        'initial_body': lead['initial_body']
                    }
                }
            )

        merge_to = lead['master']

        for slave_id in lead['dups']:
            if merge_to != slave_id:
                click.echo('Merging {} to {} for {}'.format(
                    slave_id, merge_to, lead['email'])
                )
                sf.merge(merge_to, slave_id)

        click.echo('Updating data {}'.format(lead['master']))

        strip_body = deepcopy(lead['body'])
        for i in ignored_fields + ['OwnerId']:
            if i in strip_body:
                del strip_body[i]

        if strip_body:
            sf.Lead.update(lead['master'], strip_body)

        db.leads.update(
            query,
            {
                '$set': {
                    'state': LeadState.DONE
                }
            }
        )
    finally:
        if lock == 1:
            r.delete(lock_key)


def wipe():
    db.leads.drop()


def mergeall():
    while True:
        something = False

        for lead in db.leads.find({
            'state': LeadState.START
        }).limit(10):
            something = True
            try:
                mergeone(lead)
            except SystemExit:
                continue

        if not something:
            break


def get_field_value(leadid, fields):
    lead = sf.Lead.get(leadid)
    for field in fields.split(','):
        if field in lead:
            click.echo('Field {} = {}'.format(field, lead[field]))
        else:
            click.echo("Field {} doesn't exist.".format(field))


def get_field_value_by_email(email, field):
    records = sf.query_all(queries.GET_BY_EMAIL.format(
        email=escape(email)
    ))['records']

    if records:
        for record in records:
            get_field_value(record['Id'], field)
    else:
        click.echo('No lead for email = {}'.format(email))


def get_dups_count(email):
    records = sf.query_all(queries.GET_BY_EMAIL.format(
        email=escape(email)
    ))['records']

    click.echo('Amount of dups for email {} is {}'.format(email, len(records)))


def dump_from_db(email, fields, dups=True):
    lead = db.leads.find_one({'email': email})

    if not lead:
        click.echo('No data for {}'.format(email))
        return

    if dups:
        dups = lead['dups']
    else:
        dups = {lead['master']: lead['body']}

    for dup_id, dup in dups.iteritems():
        dup['Id'] = dup_id
        for field in fields.split(','):
            if field in dup:
                click.echo('ID = {}. Field {} = {}'.format(
                    dup['Id'], field, dup[field])
                )
            else:
                click.echo("Field {} doesn't exist.".format(field))


def count_records():
    counter = 0
    for lead in db.leads.find({
        'state': LeadState.DONE
    }):
        counter += len(lead['dups'])

    click.echo('Amount of merged records {}'.format(counter))


def dump_all_from_db(fields):
    post_file = open(config.CSV_POST_UPDATE_FILE, 'w+')
    writer = csv.writer(post_file, delimiter=b',')
    writer.writerow(['Id', 'Email'] + fields.split(','))
    for lead in db.leads.find({'state': LeadState.DONE}):
        body = lead['body']
        row = [lead['master'], lead['email']]
        for field in fields.split(','):
            if field in body:
                click.echo('{}: Field {} = {}'.format(
                    lead['email'], field, body[field])
                )
                row.append(body[field])
            else:
                click.echo("Field {} doesn't exist.".format(field))
                click.echo("Skip user {}.".format(lead['email']))
                continue
            writer.writerow(row)


def delete(email):
    records = sf.query_all(queries.GET_BY_EMAIL.format(
        email=escape(email)
    ))['records']

    if records:
        click.echo('Found {} leads for email = {}'.format(len(records), email))
        for record in records:
            sf.Lead.delete(record['Id'])
            click.echo('Delete Lead {} for {}'.format(record['Id'], email))
    else:
        click.echo('No lead for email = {}'.format(email))


def test_records():
    docs = []
    csv_file = open(config.CSV_TEST_INPUT_FILE, 'rb')
    csv_reader = csv.reader(csv_file, delimiter=b',')
    next(csv_reader)
    field_names = next(csv_reader)

    for row in csv_reader:
        doc = {}
        for index, value in enumerate(row):
            field_name = field_names[index]
            if field_name:
                if str(value) == str('FALSE'):
                    value = False
                elif str(value) == str('TRUE'):
                    value = True

                doc[field_name] = value
        docs.append(doc)

    keyfunc = lambda x: x['Email']
    data = sorted(docs, key=keyfunc)

    report_file = open(config.CSV_TEST_OUTPUT_FILE, 'w+')
    report = csv.writer(report_file, delimiter=b',')

    report.writerow(field_names)

    for k, g in groupby(data, keyfunc):
        dups = {i['Id']: i for i in g}

        email = k
        master_id, slaves_ids, body, initial_body = use_rules(dups)

        click.echo(
            'Email = {}: master id = {}, slaves count = {}'.format(
                email, master_id, len(slaves_ids)
            )
        )

        row = [body.get(i, '') for i in field_names]
        report.writerow(row)


def load(email):
    records = sf.query_all(queries.GET_BY_EMAIL.format(
        email=escape(email)
    ))['records']

    for record in records:
        lead = sf.Lead.get(record['Id'])
        click.echo(lead)
