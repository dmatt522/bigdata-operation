from __future__ import division, unicode_literals

import click

from app import report
from document import fields, mappers


def status_rule(doc_dups, master_id, slave_ids):
    master_doc = doc_dups[master_id]
    if master_doc.sales_ready_lead and master_doc.has('status'):
        return master_doc.status, True

    STATUS_PRIORITIES = [
        'Sales Accepted',
        'Re-engaged',
        'Potential Opportunity',
        'Meeting Set',
        'Pursuing',
        'Untouched',
        'New',
        'Marketing Nurture',
        'Parked',
        'Send to TPR',
        'Converted',
        'Closed',
        'Manual Contact'
    ]
    statuses = [
        doc_dup.status for doc_dup in doc_dups.itervalues()
        if doc_dup.has('status')
    ]

    for status_priority in STATUS_PRIORITIES:
        if status_priority in statuses:
            return status_priority, True

    return None, True


def lead_score_rule(doc_dups, master_id, slave_ids):
    master_doc = doc_dups[master_id]
    if master_doc.sales_ready_lead and master_doc.has('lead_score'):
        return master_doc.lead_score, True

    lead_scores = [i.lead_score for i in doc_dups.itervalues()
                   if i.has('lead_score')]

    for i in doc_dups.itervalues():
        if i.has_raw('mkto2__Lead_Score__c'):
            click.echo(
                'Real lead score = {} for {}'.format(
                    i.get_raw('mkto2__Lead_Score__c'),
                    master_doc.email
                )
            )

    if not lead_scores:
        click.echo('Lead scores: no data for {}'.format(master_doc.email))
        return 0, True

    click.echo('Lead scores: {} for {}'.format(
        lead_scores,
        master_doc.email)
    )

    if any([i >= 99 for i in lead_scores]):
        return max(lead_scores), True
    elif all([i < 99 for i in lead_scores]) and sum(lead_scores) >= 99:
        report.writerow(['lead_score_sum_99', master_doc.email])
        return None, False
    else:
        # Take the average.
        return max(lead_scores), True


def lead_source_rule(doc_dups, master_id, slave_ids):
    master_doc = doc_dups[master_id]
    if master_doc.sales_ready_lead and master_doc.has('lead_source'):
        return master_doc.lead_source, True

    lead_sources = [(i.created_date, i.lead_source)
                    for i in doc_dups.itervalues()
                    if i.has('lead_source')]
    sorted_lead_sources = sorted(
        lead_sources,
        key=lambda x: x[0],
        reverse=False
    )

    if not sorted_lead_sources:
        return None, True

    return sorted_lead_sources[0][1], True


def lead_owner_rule(doc_dups, master_id, slave_ids):
    master_doc = doc_dups[master_id]
    if master_doc.sales_ready_lead and master_doc.has('lead_owner'):
        return master_doc.lead_owner, True

    owners = [(i, j.lead_owner, j.last_modified_date)
              for i, j in doc_dups.iteritems() if j.has('lead_owner')]
    sorted_owners = sorted(owners, key=lambda x: x[2], reverse=True)
    user_owners = [i for i in sorted_owners if i[1].startswith('005')]
    queue_owners = [i for i in sorted_owners if i[1].startswith('00G')]
    other_owners = [i for i in sorted_owners if
                    (not i[1].startswith('005') and
                     not i[1].startswith('00G'))]

    if user_owners:
        if len(set([i[1] for i in user_owners])) > 1:
            report_user = [(i[0], i[1]) for i in user_owners]
            report.writerow(['many_user_owners', report_user])
            return None, False

        if any([i[0] == master_id for i in user_owners]):
            return master_doc.lead_owner, True
        else:
            return user_owners[0][1], True
    elif queue_owners:
        if any([i[0] == master_id for i in queue_owners]):
            return master_doc.lead_owner, True
        else:
            return queue_owners[0][1], True
    elif other_owners:
        report_other = [(i[0], i[1]) for i in other_owners]
        report.writerow(['strange_lead_owner', report_other])
        return None, False
    else:
        return None, True


def last_interesting_type_rule(doc_dups, master_id, slave_ids):
    all_types = [(i, j.last_interesting_type, j.last_modified_date)
                 for i, j in doc_dups.iteritems()
                 if j.has('last_interesting_type')]
    sorted_all_types = sorted(all_types, key=lambda x: x[2], reverse=True)
    caw_types = [i for i in sorted_all_types if i[1] == 'CAW']
    other_types = [i for i in sorted_all_types if i[1] != 'CAW']
    master_doc = doc_dups[master_id]

    if caw_types:
        if any([i[0] == master_id for i in caw_types]):
            return master_doc.last_interesting_type, True
        else:
            return caw_types[0][1], True
    elif other_types:
        if any([i[0] == master_id for i in other_types]):
            return master_doc.last_interesting_type, True
        else:
            return other_types[0][1], True
    else:
        return None, True


def apply_rule(special_field, doc_dups, master_id, slave_ids):
    """Factory of special rules."""
    func_map = {
        'status': status_rule,
        'lead_score': lead_score_rule,
        'lead_source': lead_source_rule,
        'lead_owner': lead_owner_rule,
        'last_interesting_type': last_interesting_type_rule
    }
    value, status = func_map[special_field](doc_dups, master_id, slave_ids)

    if value is None:
        result = {}
    else:
        field_metadata = fields[special_field]
        field_name = field_metadata[0]
        field_type = field_metadata[1]
        field_value = mappers[field_type][1](value)
        result = {field_name: field_value}

    return result, status
