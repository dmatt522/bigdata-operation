from __future__ import division, unicode_literals

import csv

import config
from utils import Salesforce, get_sf_connection, get_mongo, get_redis


sf = Salesforce(**get_sf_connection())
db = get_mongo()
r = get_redis()
report_file = open(config.CSV_EXCEPTIONS_FILE, 'a')
report = csv.writer(report_file, delimiter=b',')
