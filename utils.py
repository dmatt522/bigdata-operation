from __future__ import division, unicode_literals

import redis
from pymongo import MongoClient
from mysql.connector import conversion
from simple_salesforce import Salesforce as BaseSalesforce
from simple_salesforce.api import SalesforceGeneralError

import config

escape = conversion.MySQLConverter().escape


def get_sf_connection():
    connection = {
        'username': config.SALESFORCE_USERNAME,
        'password': config.SALESFORCE_PASSWORD,
        'organizationId': config.SALESFORCE_ORG_ID,
        'sandbox': config.SALESFORCE_SANDBOX,
        'sf_version': '34.0'
    }

    if config.USE_PROXY:
        connection['proxies'] = config.SALESFORCE_PROXIES

    return connection


def setup_indexes(db):
    db.leads.ensure_index('email', unique=True)
    db.leads.ensure_index('state')


def get_mongo():
    mongo = MongoClient()
    db = mongo[config.SALESFORCE_MONGO_DB]
    setup_indexes(db)

    return db


def get_redis():
    r = redis.StrictRedis(db=config.SALESFORCE_REDIS_DB)

    return r


class Salesforce(
    BaseSalesforce
):
    def merge(self, master, slave):
        self.session_id, self.sf_instance

        url = 'https://{sf_instance}/services/Soap/u/{sf_version}'

        url = url.format(
            sf_instance=self.sf_instance,
            sf_version=self.sf_version
        )

        headers = self.headers.copy()

        headers.update({
            'content-type': 'text/xml; charset=utf-8',
            'SOAPAction': 'merge'
        })

        body = '''<?xml version="1.0" encoding="utf-8"?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
  xmlns:urn="urn:partner.soap.sforce.com"
  xmlns:urn1="urn:sobject.partner.soap.sforce.com">
  <soapenv:Header>
     <urn:SessionHeader>
        <urn:sessionId>{sessionId}</urn:sessionId>
     </urn:SessionHeader>
  </soapenv:Header>
  <soapenv:Body>
     <urn:merge>
        <urn:request>
           <urn:masterRecord>
              <urn1:type>Lead</urn1:type>
              <urn1:Id>{masterRecord}</urn1:Id>
              <!--You may enter ANY elements to update at this point-->
              <Description>Merged with Dupe Lead.</Description>
           </urn:masterRecord>
           <urn:recordToMergeIds>{recordToMergeIds}</urn:recordToMergeIds>
        </urn:request>
     </urn:merge>
  </soapenv:Body>
</soapenv:Envelope>'''

        body = body.format(
            sessionId=self.session_id,
            masterRecord=master,
            recordToMergeIds=slave
        )

        result = self.request.post(
            url,
            data=body,
            headers=headers
        )

        if result.status_code != 200:
            raise SalesforceGeneralError(
                url,
                'describe',
                result.status_code,
                result.content
            )


class LeadState(object):
    READY = 5
    START = 0

    DONE = 10
    CAPACITY = 9
    SINGLE = 8
    EMPTY = 7
